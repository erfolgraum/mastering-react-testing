import React from "react";
import "./App.css";
import CommunityPage from "./pages/CommunityPage";
import JoinProgramPage from "./pages/JoinProgramPage";
import Home from "./pages/Home";
import UserDetail from "./components/UserDetail";
import NotFoundPage from "./pages/NotFoundPage";

import { Routes, Route, Navigate } from "react-router-dom";



function App() {
  return (
    <div className="App">
      <div id="app-container">
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/community" element={<CommunityPage />} />
          <Route path="/community/:userId" element={<UserDetail />} />
          <Route path="/join" element={<JoinProgramPage />} />
          <Route path="/not-found" element={<NotFoundPage />}/>
          <Route path="*" element={<Navigate to="/not-found" /> } />
        </Routes>
      </div>
    </div>
  );
}

export default App;
