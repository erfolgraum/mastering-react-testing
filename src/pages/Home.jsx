import React from 'react'
import { Link } from 'react-router-dom'


const Home = () => {
  return (
    <div>
      <header>
          <Link to="/" >HOME PAGE</Link>
          <Link to="/community" >OUR COMMUNITY</Link>
          <Link to="/join" >JOIN OUR PROGRAM</Link>
        </header>
    </div>
  )
}

export default Home

