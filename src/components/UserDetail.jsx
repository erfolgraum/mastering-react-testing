import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import NotFoundPage from "../pages/NotFoundPage";

export const UserDetail = () => {
  const [user, setUser] = useState();

  const { userId } = useParams();
  console.log("Route-params", userId);


  useEffect(() => {
    fetch(`/community/${userId}`)
      .then((res) => {
        return res.json();
      })
      .then((user) => {
        setUser(user);
      });
  }, [userId]);

  return user ? (
      <div className="user user--id">
        <h1>Nice to meet you</h1>
        <div className="user" key={user.id}>
          <img src={user.avatar} className="user__img" alt={user.id} />
          <p className="user__desc">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolor.
          </p>
          <h4 className="user__name">
            {user.firstName} {user.lastName}
          </h4>
          <div className="user__position">{user.position}</div>
        </div>
      </div>
  ) : <NotFoundPage />
};

export default UserDetail;
